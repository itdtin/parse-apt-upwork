import requests
from config import DataForRequest, time_sec
from Create_DB_TB import EntryDataIntoBD, MyDBHousesUpwork
from bs4 import BeautifulSoup
import json
import csv


def read_csv() -> list:
    list = []
    with open('locations_data.csv', 'r') as fp:
        reader = csv.reader(fp, delimiter=',', quotechar='"')
        for row in reader:
            data_row1 = row
            list.append(data_row1)
    return list


def session_init(data: dict):
    """Initiate session with headers
    :param data: headers for request"""
    s = requests.Session()
    s.headers.update(data)
    return s


def request_html(url: str, session, data: dict):
    """Get html for add's page
    :param url: url
    :param session: initiated session with headers
    :param data: payload data for post request"""
    # need to modify request's data in get_payload_data
    response = session.post(url, json=data)
    return response


def requests_get(url: str):
    """Get html from page with one ad"""
    data = DataForRequest()
    s = session_init(data.get_headers_for_ads(url))
    response = s.get(url)
    return response.text


@time_sec
def parse_links(json: dict) -> list:
    """Get links for ads"""
    r = json['results']['property']['items']
    ads_from_city = []
    for j in r:
        ads_from_city.append('https://www.realtor.com' + r[j]['ldpUrl'])
    return ads_from_city


def parse_all_ads_links(query: list) -> list:
    """Parse all ads links for one city in state
    :param query: list with city-[0] and state-[2]"""
    url = 'https://www.realtor.com/search_result.json'
    url_count = 'https://www.realtor.com/search_result_count'
    data = DataForRequest()
    ads_from_city = []
    data_for_req = {'city': query[0], 'state': query[2]}

    s = session_init(data.get_main_headers(data_for_req))  # session init
    count_ads_for_city = request_html(url_count, s, data.get_data_to_count(data_for_req)).json()['properties_count']
    print("Founded {} ads in the {} city, state ".format(count_ads_for_city, query[0], query[2]))
    pages = int(count_ads_for_city) // 44
    for i in range(1, 2):#pages + 1):
        print(data_for_req, 'Now on page {}'.format(i))
        data_for_req.update({'page': i, 'items': int(count_ads_for_city)})
        response = request_html(url, s, data.get_payload_data(data_for_req)).json()  # response json from 1 page
        ads_from_city.extend(parse_links(response))
    return ads_from_city


@time_sec
def parse_ad(url: str) -> dict:
    """Parse ad"""
    html = requests_get(url)
    soup = BeautifulSoup(html, 'lxml')
    addresses = soup.find('h1', attrs={'itemprop': "address"})
    streetAddress = addresses.find('span', attrs={'itemprop': "streetAddress"}).getText().strip()
    cityAddress = addresses.find('span', attrs={'itemprop': "addressLocality"}).getText().strip()
    stateAddress = addresses.find('span', attrs={'itemprop': "addressRegion"}).getText().strip()
    postalCode = addresses.find('span', attrs={'itemprop': "postalCode"}).getText().strip()
    price = soup.find('span', attrs={'itemprop': "price"}).get('content')
    sqft = int(soup.find('li', attrs={'data-label': "property-meta-sqft"}).find('span').getText().strip().replace(',',''))
    sqft_lot_name = soup.find('li', attrs={'data-label': "property-meta-lotsize"}).getText().split('\n')[-2].strip()
    sqft_lot = float(soup.find('li', attrs={'data-label': "property-meta-lotsize"}).
                         find('span').getText().strip().replace(',',''))
    area = ''
    beds = soup.find('li', attrs={'data-label': "property-meta-beds"}).find('span').getText().strip()
    baths = soup.find('li', attrs={'data-label': "property-meta-bath"}).find('span').getText().strip()
    description = soup.find('p', attrs={'id': "ldp-detail-romance"}).getText().strip().replace('w/ ', '')
    ##### Additional information #####
    ht = [i.getText().strip() for i in soup.find('div', attrs={'class': 'listing-subsection-features'}).
        find_all('h4', attrs={'class':'title-subsection-sm'})]
    dv = [i.getText().strip().replace('\n\n\n\n\n', ', ').replace('\n', ', ')
          for i in soup.find('div', attrs={'class': 'listing-subsection-features'}).
                       find_all('div', attrs={'class': 'row'})[1:]]
    additional_details = dict(zip(ht, dv))
    ##### Images #####
    img = soup.find_all('div', attrs={'class': "list-gallery-wrapper"})
    images = {}
    count = 1
    for i in img:
        a = i.find('img').get('data-src')
        if a is not None:
            images.update({'img{}'.format(count): a})
            count += 1
    ##### End Images #####
    source_id = 1
    property_type = soup.find('input', attrs={'name': 'prop_type'}).get('value')
    status = soup.find('input', attrs={'name': 'prop_status'}).get('value')

    data = {'url': url,
            'address': streetAddress,
            'city_id': cityAddress,
            'state': stateAddress,
            'postal_code': postalCode,
            'price': price,
            'sqft': sqft,
            'sqft_lot': sqft_lot,
            'sqft_lot_name': sqft_lot_name,
            'area': area,
            'beds': beds,
            'baths': baths,
            'description': description,
            'additional_details': json.dumps(additional_details),
            'images': json.dumps(images),
            'source_id': source_id,
            'property_type': property_type,
            'status': status
            }
    return data


def main():
    for city in read_csv(): # we can change data, that entry
        urls = parse_all_ads_links(city) # script parse all ads links for one city
        for url in urls:
            d = parse_ad(url) # script parse all information  for ine ad
            d.update({'city_id': city[0]})
            EntryDataIntoBD().entry_into_properties(d) # script entry data into db



if __name__ == '__main__':
    main()
