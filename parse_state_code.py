import csv
from bs4 import BeautifulSoup
from time import sleep
from datetime import datetime
import requests
import re
from config import *
from time import sleep
import string


def write_csv(data):
    row = tuple(data)
    with open('locations_data.csv', 'a') as file:
        writer = csv.writer(file)
        writer.writerow(row)


def read_csv():
    list = []
    with open('LIST CITIES.csv', 'r') as fp:
        reader = csv.reader(fp, delimiter=',', quotechar='"')
        for row in reader:
            data_row1 = row
            list.append(data_row1)
    return list


def parse_state_code():
    r = requests.get('https://www.factmonster.com/us/postal-information/state-abbreviations-and-state-postal-codes')
    html = r.text
    soup = BeautifulSoup(html,'lxml')
    a = soup.find_all('tr')
    list = []
    for i in a:
        td = i.find_all('td')
        if len(td) != 0:
            state_name = td[0].getText()
            state_code = td[2].getText()
            list.append([state_name, state_code])
    return list


def main():
    url = 'https://www.realtor.com/search_result.json'
    state_codes = parse_state_code()
    print(state_codes)
    from_csv = read_csv()
    print(from_csv)
    for i in from_csv:
        for j in state_codes:
            if i[1].strip() == j[0].strip():
                data = i
                data.append(j[1])
                write_csv(data)
            elif i[1].strip() in j[0].strip():
                print(i, j)


if __name__ == '__main__':
    main()


