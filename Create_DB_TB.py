import mysql.connector
from config import get_sql_data, location_file
from time import sleep
import json
import csv


class EntryDataIntoBD(object):
    sql_data: dict = get_sql_data()
    location_file = location_file

    def read_csv(self) -> list:
        list = []
        with open(self.location_file, 'r') as fp:
            reader = csv.reader(fp, delimiter=',', quotechar='"')
            for row in reader:
                data_row = row
                list.append(data_row)
        return list

    def connect_to_db_houses(self):
        """Connect to my db called houses_upwork"""
        try:
            print('Connecting to MySQL database...')
            mydb = mysql.connector.MySQLConnection(host="{}".format(self.sql_data['host']),
                                                   user="{}".format(self.sql_data['user']),
                                                   passwd="{}".format(self.sql_data['psw']),
                                                   database="{}".format(self.sql_data['db_name']))
            if mydb.is_connected():
                print('Connection established.')
                return mydb
        except mysql.connector.errors.Error as error:
            print(error)

    def entry_into_properties(self, data):
        mydb = self.connect_to_db_houses()
        mycursor = mydb.cursor()
        sql = "SELECT * FROM Properties WHERE url = '{}'".format(data["url"])
        mycursor.execute(sql)
        rows = mycursor.fetchall()
        mycursor.close()
        if len(rows) == 0:
            sql = "SELECT id FROM Cities WHERE city_name = '{}' AND state_code = '{}'".format(
                data['city_id'], data['state'])
            sql2 = "SELECT id FROM Sites WHERE url = '{}'".format('https://www.realtor.com/')
            mycursor3 = mydb.cursor()
            mycursor3.execute(sql)
            city_id = mycursor3.fetchone()[0] #Get id from cities table for field city_id
            mycursor3.execute(sql2)
            source_id = mycursor3.fetchone()[0]
            mycursor3.close()
            data.update({'city_id': city_id, 'source_id': source_id})
            row = tuple(data.values())
            sql = "INSERT INTO Properties (" \
                  "url, address, city_id, state, " \
                  "postal_code, price, sqft, sqft_lot, sqft_lot_name," \
                  "area, beds, baths, description," \
                  "additional_details, images, source_id," \
                  "property_type, status) VALUES (" \
                  "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            mycursor2 = mydb.cursor()
            mycursor2.execute(sql, row)
            mydb.commit()
        else:
            print('This url has been previously added.')

    def entry_into_cities(self):
        """Entry data into cities table"""
        mydb = self.connect_to_db_houses()
        loc_list = self.read_csv()
        for i in loc_list:
            sql = "SELECT * FROM Cities WHERE city_name = '{}' AND state_code = '{}'".format(i[0], i[2])
            mycursor = mydb.cursor()
            mycursor.execute(sql)
            rows = mycursor.fetchall()
            mycursor.close()
            if len(rows) == 0:
                row = []
                row.append(i[0])
                row.append(i[2])
                row = tuple(row)
                sql = "INSERT INTO Cities (city_name, state_code) VALUES (%s, %s)"
                mycursor2 = mydb.cursor()
                mycursor2.execute(sql, row)
                mydb.commit()
            else:
                print('This url has been previously added.')


class MyDBHousesUpwork(object):
    sql_data: dict = get_sql_data()

    def __init__(self):
        self.create_db()
        try:
            self.create_table_cities()
        except mysql.connector.errors.ProgrammingError:
            print('Таблица Cities уже существует')
        try:
            self.create_table_sites()
        except mysql.connector.errors.ProgrammingError:
            print('Таблица Sites уже существует')
        try:
            self.create_table_properties()
        except mysql.connector.errors.ProgrammingError:
            print('Таблица Properties уже существует')

    def connect_to_db_houses(self):
        """Connect to my db called houses_upwork"""
        try:
            print('Connecting to MySQL database...')
            mydb = mysql.connector.connect(host="{}".format(self.sql_data['host']),
                                           user="{}".format(self.sql_data['user']),
                                           passwd="{}".format(self.sql_data['psw']),
                                           database="{}".format(self.sql_data['db_name']))
            if mydb.is_connected():
                print('Connection established.')
                return mydb
        except mysql.connector.errors.Error as error:
            print(error)

    def create_db(self):
        """Create db"""

        mydb = mysql.connector.connect(host="{}".format(self.sql_data['host']), user="{}".format(self.sql_data['user']),
                                       passwd="{}".format(self.sql_data['psw']))
        mycursor = mydb.cursor()
        mycursor.execute("CREATE DATABASE IF NOT EXISTS {}".format(self.sql_data['db_name']))
        mycursor.close()
        mydb.close()

    def create_table_cities(self):
        """Create table Cities and indexes"""

        mydb = self.connect_to_db_houses()
        mycursor = mydb.cursor()
        mycursor.execute("CREATE TABLE Cities ("
                         "id BIGINT(20) AUTO_INCREMENT PRIMARY KEY COMMENT "
                         "'ID of the City (Foreign key is set in the Properties table)', "
                         "city_name VARCHAR(255) COMMENT 'Full name of the city', "
                         "state_code VARCHAR(2) COMMENT 'State code of the city')")
        mycursor.execute("CREATE INDEX state_code ON Cities (state_code)")
        mycursor.execute("CREATE INDEX name ON Cities (city_name)")
        mydb.close()
        mycursor.close()

    def create_table_sites(self):
        """Create table Sites and indexes"""

        mydb = self.connect_to_db_houses()
        mycursor = mydb.cursor()
        mycursor.execute("CREATE TABLE Sites ("
                         "id BIGINT(20) AUTO_INCREMENT PRIMARY KEY COMMENT 'Auto increment ID', "
                         "url VARCHAR(1024) COMMENT 'Full URL of the website from which the data will be scrapped', "
                         "source VARCHAR(32) COMMENT 'Website name',"
                         "amount BIGINT(20) COMMENT 'Minimum amount for to scrape data')")
        # create indexes
        mycursor.execute("CREATE INDEX price ON Sites (amount)")  # This index no normal i have a question to customer
        mycursor.execute("CREATE INDEX website ON Sites (source)")
        mycursor.execute("CREATE INDEX amount ON Sites (amount)")

    def create_table_properties(self):
        """Create table Properties and indexes"""

        mydb = self.connect_to_db_houses()
        mycursor = mydb.cursor()
        mycursor.execute(
            "CREATE TABLE Properties ("
            "id BIGINT(20) AUTO_INCREMENT PRIMARY KEY, "
            "url VARCHAR(1024) COMMENT 'Full URL of the website from which the data was scrapped', "
            "address VARCHAR(1024) COMMENT 'Full adress of the property',"
            "city_id BIGINT(20) COMMENT 'ID of the city from cities table',"  # Foreign key to id in table Cities
            "state VARCHAR(2) COMMENT 'State code',"
            "postal_code VARCHAR(15) COMMENT 'Postal code/ Zip code',"
            "price DOUBLE COMMENT 'Price in numbers only',"
            "sqft INT(10) COMMENT 'Square Feet in numbers only',"
            "sqft_lot INT(10) COMMENT 'Lot size in numbers only',"
            "sqft_lot_name VARCHAR(10), COMMENT 'Name sq'"
            "area VARCHAR(128) COMMENT 'Extra details of the area',"
            "beds INT(5) COMMENT 'Total Beds Count',"
            "baths INT(5) COMMENT 'Total Baths Count',"
            "description LONGTEXT COMMENT 'Full description of the property',"
            "additional_details LONGTEXT COMMENT "
            "'Any additional details which can be scrapped from the web page in text or HTML',"
            "images LONGTEXT COMMENT 'Full URL of images in JSON format. Can be multiple.',"
            "source_id BIGINT(20) COMMENT 'Website/Source ID from the sites table',"  # Foreign key to id in table Sites
            "property_type VARCHAR(32) COMMENT 'Type of the property(Condo)',"
            "status VARCHAR(32) COMMENT 'Status of the property',"
            "created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Timestamp of the created record')")
        mycursor.execute("alter table Properties add foreign key (city_id) references Cities(id)")
        mycursor.execute("alter table Properties add foreign key (source_id) references Sites(id)")
        #create indexes
        mycursor.execute("CREATE INDEX state ON Properties (state)")
        mycursor.execute("CREATE INDEX city ON Properties (city_id)")
        mycursor.execute("CREATE INDEX postal_code ON Properties (postal_code)")
        mycursor.execute("CREATE INDEX price ON Properties (price)")
        mycursor.execute("CREATE INDEX sqrt ON Properties (sqft)")
        mycursor.execute("CREATE INDEX sqrt_lot ON Properties (sqft_lot)")
        mycursor.execute("CREATE INDEX baths ON Properties (baths)")
        mycursor.execute("CREATE INDEX beds ON Properties (beds)")
        mycursor.execute("CREATE INDEX website ON Properties (source_id)")
        mycursor.execute("CREATE INDEX property_type ON Properties (property_type)")
        mycursor.execute("CREATE INDEX status ON Properties (status)")


def main():
    MyDBHousesUpwork()


if __name__ == '__main__':
    main()
