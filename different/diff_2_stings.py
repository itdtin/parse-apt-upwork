
def diff(a: str, b: str) -> bool:
    """Differences 2 strings letters"""
    aset = set(a)
    bset = set(b)
    print(aset)
    print(bset)
    if len(aset) == len(bset):
        for i in aset:
            if i not in bset:
                return False
            else:
                return True
    else:
        return False


def main():
    a = 'Roman, i love you!'
    b = 'Ksenia, i love you too!'
    diff(a, b)


if __name__ == '__main__':
    main()