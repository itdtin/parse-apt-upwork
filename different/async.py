import asyncio
import time


def time_sec(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        returned_value = func(*args, **kwargs)
        end = time.time()
        print('Время выполнения: {}'.format(end - start))
        return returned_value
    return wrapper


@time_sec
async def foo():
    print('Running in foo')
    print('Выходим из foo')
    await asyncio.sleep(0)
    print('Вошли в foo опять')
    print('Explicit context switch to foo again')


@time_sec
async def bar():
    print('Вошли в bar')
    print('Explicit context to bar')
    await asyncio.sleep(0)
    print('Implicit context switch back to bar')


ioloop = asyncio.get_event_loop()
tasks = [ioloop.create_task(foo()), ioloop.create_task(bar())]
wait_tasks = asyncio.wait(tasks)
ioloop.run_until_complete(wait_tasks)
ioloop.close()