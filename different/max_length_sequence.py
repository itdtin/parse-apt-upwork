
def max_l(a: list) -> bool:
    current = 0
    best = 0
    for i in a:
        if i > 0:
            current += 1
            best = max(best, current)
        else:
            current = 0
    return best


def main():
    a = [0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1]
    print(max_l(a))


if __name__ == '__main__':
    main()