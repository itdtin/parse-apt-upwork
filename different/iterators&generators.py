## generator


def simple_gen(val):
    while val > 0:
        val -= 1
        yield 1


def main():
    simple_gen(10)



if __name__ == '__main__':
    main()