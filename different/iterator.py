class SimpleIterator:
    def __init__(self, limit):
        self.limit = limit
        self.counter = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.counter < self.limit:
            self.counter += 1
            return 1
        else:
            raise StopIteration


def main():
    s_it = SimpleIterator(5)
    for i in s_it:
        print(s_it.counter)

if __name__ == '__main__':

    main()
