location_file = 'locations_data.csv'
table_names = ['Properties', 'Cities', 'Sites']
sum_query = "300000"


def get_sql_data() -> dict:
    #################################### Enter your data here ################################

    HOST = '127.0.0.1'
    USER = 'root'
    PSW = 'user00fe0a'
    #################################### End enter ################################

    DB_NAME = 'houses_upwork'
    DB_DATA = {'host': HOST, 'user': USER, 'psw': PSW, 'db_name': DB_NAME}
    return DB_DATA


def time_sec(func):
    def wrapper(*args, **kwargs):
        import time
        start = time.time()
        return_value = func(*args, **kwargs)
        end = time.time()
        print('Время выполнения: {}'.format(end - start))
        return return_value

    return wrapper


class DataForRequest(object):
    facets = {
        "beds_min": None,
        "beds_max": None,
        "baths_min": None,
        "baths_max": None,
        "price_min": sum_query,
        "prop_type": "single-family-home",
        "sqft_min": None,
        "sqft_max": None,
        "acre_max": None,
        "lot_unit": None,
        "age_max": None,
        "age_min": None,
        "radius": None,
        "pets": [],
        "days_on_market": "",
        "open_house": None,
        "show_listings": "",
        "pending": None,
        "foreclosure": None,
        "new_construction": None,
        "keywords": [],
        "multi_search": {},
        "include_pending_contingency": False,
        "features_hash": []}
    url = 'https://www.realtor.com/'
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) ' \
                 'Chrome/79.0.3945.117 Safari/537.36'
    accept_encoding = 'gzip, deflate, br'
    accept_language = 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
    sec_fetch_mode = 'cors'
    authority = 'www.realtor.com'
    scheme = 'https'
    cookie = '__vst=5e0205d6-491a-486d-a0b1-a6fd2a6324a7; __ssnstarttime=1579439698; ajs_anonymous_id=%22f77520ae-8fb5-4e80-a791-b1f4a50d00f1%22; _tac=false~self|not-available; _ta=us~1~89df68bc35032d48c293adce8574aa42; QSI_SI_cN5gYlOK8LtV3pz_intercept=true; split=n; split_tcv=127; __split=93; ab.storage.userId.7cc9d032-9d6d-44cf-a8f5-d276489af322=%7B%22g%22%3A%22visitor_5e0205d6-491a-486d-a0b1-a6fd2a6324a7%22%2C%22c%22%3A1579439810747%2C%22l%22%3A1579439810747%7D; ab.storage.sessionId.7cc9d032-9d6d-44cf-a8f5-d276489af322=%7B%22g%22%3A%2268e57f7a-095c-9d69-e5b5-990870d16b38%22%2C%22e%22%3A1579441610751%2C%22c%22%3A1579439810752%2C%22l%22%3A1579439810752%7D; G_ENABLED_IDPS=google; AMCVS_8853394255142B6A0A4C98A4%40AdobeOrg=1; s_ecid=MCMID%7C73909304952060467233557495383980138158; ajs_user_id=null; ajs_group_id=null; AMCVS_AMCV_8853394255142B6A0A4C98A4%40AdobeOrg=1; AMCV_AMCV_8853394255142B6A0A4C98A4%40AdobeOrg=-1712354808%7CMCMID%7C70976730768191440119221078534746640466%7CMCOPTOUT-1579447013s%7CNONE%7CvVersion%7C4.3.0; _ga=GA1.2.1927059241.1579439812; _gid=GA1.2.2138134346.1579439813; threshold_value=67; automation=false; clstr=v; clstr_tcv=58; bcc=false; bcvariation=SRPBCRR%3Av1%3Adesktop; ab_srp_viewtype=ab-list-view; userStatus=return_user; user_canceled_one_tap=true; _ncg_id_=16f9c929884-43d4de92-67e1-4928-89bc-756d4d537d1f; __qca=P0-648059592-1579451019648; _ncg_g_id_=4f53b8d0-3f43-4ca8-bab0-ec7c6e73c365; QSI_SI_6DNTqAMybsoeO2N_intercept=true; last_view_timestamp=1579645812957; existing_timestamp=1579652003452; user_activity=return; last_ran=1579653354109; url_search_data=search_city%3DYork%26search_state_id%3DPA; header_slugs=gs%3DYork_PA%26lo%3DYork%26st%3Dcity%2Cgs%3DYork-County_PA%26lo%3DYork%26st%3Dcity; criteria=loc%3DYork%2C+PA%26locSlug%3DYork_PA%26lat%3D39.9647979%26long%3D-76.7318403%26status%3D1%26pg%3D1%26pgsz%3D44%26sprefix%3D%2Frealestateandhomes-search%26city%3DYork%26state_id%3DPA%26county_fips%3D42133%26county_fips_multi%3D42133; srchID=a6d00d2e4606417e91c3c5dafc25a1fa; AMCV_8853394255142B6A0A4C98A4%40AdobeOrg=-1712354808%7CMCIDTS%7C18283%7CMCMID%7C73909304952060467233557495383980138158%7CMCAAMLH-1580284169%7C6%7CMCAAMB-1580284169%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1579686569s%7CNONE%7CMCAID%7CNONE%7CMCSYNCSOP%7C411-18288%7CvVersion%7C4.3.0; last_viewed_property=%2Frealestateandhomes-detail%2F147-Harrison-Ave_Staten-Island_NY_10302_M46010-95059; seen_ny_prop=true; __ssn=a1f32544-3502-43f6-8a32-bfac077fc830; previousUrl=%2Frealestateandhomes-detail%2F147-Harrison-Ave_Staten-Island_NY_10302_M46010-95059; QSI_HistorySession=https%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-search%2FLees-Summit_MO%2Ftype-single-family-home%2Fprice-300000-na~1579645813383%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-search%2FO\'Fallon-Park_Saint-Louis_MO%2Ftype-single-family-home%2Fprice-300000-na~1579645911427%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-search%2FSaint-Louis_MO~1579645918206%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-search%2FO\'Fallon-Park_Saint-Louis_MO~1579645935452%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-search%2FCoeur-d\'Alene_ID%2Fschools%2FCoeur-d\'Alene-Charter-Academy-0722868161~1579646149806%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-detail%2F4319-SW-Fairview-Circus_Portland_OR_97221_M12182-33400~1579650678001%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-search%2FLos-Angeles_CA~1579652004041%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-detail%2F2160-Century-Park-E-Apt-303_Los-Angeles_CA_90067_M28623-51081%3Fview%3Dqv~1579652006646%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-search%2FLos-Angeles_CA%3F~1579653304395%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-detail%2F586-Soapstone-Ln_York_PA_17404_M49241-80356~1579653709810%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-detail%2F2160-Century-Park-E-Apt-303_Los-Angeles_CA_90067_M28623-51081~1579653859663%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-detail%2F586-Soapstone-Ln_York_PA_17404_M49241-80356~1579679380642%7Chttps%3A%2F%2Fwww.realtor.com%2Frealestateandhomes-detail%2F147-Harrison-Ave_Staten-Island_NY_10302_M46010-95059%3Fex%3DNY2911968234~1579683018064; _ncg_sp_ses.cc72=*; adcloud={%22_les_v%22:%22y%2Crealtor.com%2C1579685858%22}; _tas=fe9zv9ubnuj; _rdc-next_session=a1RRdWZkenBJV3kyOXdpVTZnaE8wZTZHS2ZWM0ZzZ0RKalM5Ym5oK2lLY0kxeFhJb25YSGR6Z0taSGM3bE5uSGlGVXF6Qi8zeHd0cStVVWprWkJuS2x5NU05WEJ6ZGQ1ZDVqejg1TWMybzU3SjFQVWRubC93ZXl2Z1ZPQ3hmRllxUHJZUTlDUExWZGZyNFBvMjgvRGZTWndYMGxNMHFyQVk3cWMvTVZxZmQ4VG9md0IwRVM0cE1xbi9KWGZGdVpJbkpCclJsZDB6WURGd202MVhtU3BSUkdJR2gxM0ZENHA1elAzYkFEUDVoTVlpRTE3R3hNcTFmcWRSWTR1QTFKcGpQd2d5ZWRDbk5TNmtlUU1WQjVHd2JvVUt4Ymx3Q2wxaHBiZWV0dVp4Q1ZGM3FKZHpqNmtPVFByWjcyS0xtNGFFOUEvNGt3VnBCdjEzMG14NG4zcytPSTJuaUh3THR5VThablVsT0puT2tTczZYVU9oRFh0ZXl6dEN6YXFyQk9Ld21weXBNS2wreU1lTFk1bXkwNkdOa2R2eHplYnZidjAzM2JFNGhqNzRON2dIR2VyQXZVQUpZMVRBQmlNaTdwalpLSFNlYU90cVdzYUNOaVpHaTFkUVdieDBOcUswaytuNkpTYmlydkowNE5oeWIwZWpCcW1sQWkvOXRTRzJ5SWxjUFlqWXN0aFlhcC9hU3hSQXBLZHBKNm1OZlhUb3ppTUdyMnAwaURIQ08xQ09IdzlEY2ZSeXdiMVBXQzk0b1J2b0p0SFgyTkhlNXlEUEV4b3NJZEJ2UW9YYmlNSy9YcjBBQ1RJcDAwY1J4dz0tLUVDM3FYeWNxdDhLRHlvZnVOOFRoVXc9PQ%3D%3D--3c5a43d42701de596cbc416d4b8a873629f1c30e; _ncg_sp_id.cc72=3a698550-65b6-4891-a2d0-106234052b59.1578832719.1.1579684064.1578832719.71925785-72b8-46e7-92b3-1219e6017b5d; __edwssnstarttime=1579684064; reese84=3:bIqBU7vyj2cmpbSkGB/AKA==:3WdW9QBpjrJyeL/vjl5+SsEVgn5n72xQeTPfi+rQVY/yOMI9c0mnxnRNTHBCvcxUTMn/mEfqpVroRA7TsPHR0z0RnmwyJWGc4CntXQEDf1PTZh08JU6TltUSv9zDZgb3RRbxyOxb2WmnrLx+5wcrC2HgwjeJ3MzZL2K0qBb+V0YB0QBt32yN9v7cVLPq+oxiWHH6RvE7QwCGoz87fwAtYgdiuyDPWpzg9hwO762w8DVPZEj7cWYMmnfGLMSe5bnfqJuCao2dW4BfC1bFbi81+3uFw24mxfQZaZNd45+bKlhAMhJFIFBOxvrLX3m0eB2bDXVr63CPfcll+CFdyCGcQR/sETDBf0DHQfsVkrZodOvJLPd37f9B1p5IpYtrDsotHFjzU+PVCybpdDJV6LjYxc2sbxL01ueXVWoX9oPrYn0rstmlvhLxdOcpOVfrVNWJ:pvzfNek8E2arGUN7+8fh24W+oHlXlUQ15rjETS3cNdM='

    def get_main_headers(self, data: dict):
        """get headers for main requests"""
        cookie = self.cookie
        authority = self.authority
        method = 'POST'
        path = '/search_result.json'
        scheme = self.scheme
        accept = 'application/json, text/javascript, */*; q=0.01'
        accept_encoding = self.accept_encoding
        accept_language = self.accept_language
        content_length = '1300'
        content_type = 'application/json'
        origin = self.url
        referer = 'https: //www.realtor.com/realestateandhomes-search/{}_{}/type-{}/price-{}-na' \
            .format(data['city'].replace(' ', '-'),
                    data['state'],
                    self.facets['prop_type'],
                    self.facets['price_min'])
        sec_fetch_mode = self.sec_fetch_mode
        sec_fetch_site = 'same-origin'
        user_agent = self.user_agent
        x_csrf_token = '8NpPGCri7gNxKJ2HgPJdV9Qz0o3fwpSDKec8MovLAKf60n6Ms6yGscjiADHpHuiAHP8V07bcMihe/dtZzC+tRQ=='
        x_newrelic_id = 'VwEPVF5XGwsIUlhRAAkH'
        x_requested_with = 'XMLHttpRequest'

        h = {'user-agent': user_agent,
             'authority': authority,
             'method': method,
             'path': path,
             'scheme': scheme,
             'accept': accept,
             'accept-encoding': accept_encoding,
             'accept-language': accept_language,
             'content-length': content_length,
             'content-type': content_type,
             'cookie': cookie,
             'origin': origin,
             'referer': referer,
             'sec-fetch-mode': sec_fetch_mode,
             'sec-fetch-site': sec_fetch_site,
             'x-csrf-token': x_csrf_token,
             'x-newrelic-id': x_newrelic_id,
             'x-requested-with': x_requested_with
             }
        return h

    def get_payload_data(self, data: dict) -> dict:
        data = {"search_criteria": "{}_{}/type-{}/price-{}-na".format(data['city'].replace(' ', '-'),
                                                                      data['state'],
                                                                      self.facets['prop_type'],
                                                                      self.facets['price_min']),
                "search_controller": "Search: : PropertiesController",
                "sort": None,
                "position": None,
                "discovery_mode": False,
                "search_type": "city",
                "filter_count": {"count_large": 0, "count_small": 1},
                "properties_count": data['items'],  # получаем из другого запроса
                "lat_long": {},  # понять как получать координаты и нужны ли они
                "city": data['city'],
                "state": data['state'],
                "postal": None,
                "county": data['city'],
                "neighborhood": None,
                "street": None,
                "school": None,
                "school_district": None,
                "university": None,
                "park": None,
                "page_size": 44,
                "page_offset": 44 * data['page'],  # меняем тут для пагинации
                "pin_height": 25,
                "viewport_height": 0,
                "facets": self.facets,
                "bounding_box": None,
                "max_clusters": 30,
                "max_pins": 44}
        return data

    def get_data_to_count(self, data: dict) -> dict:
        """get data for count paggins in one city"""
        data = {
            "search_criteria": "{}_{}".format(data['city'].replace(' ', '-'), data['state']),
            "city": data['city'],
            "county": data['city'],
            "discovery_mode": False,
            "state": data['state'],
            "postal": None,
            "sort": None,
            "position": None,
            "facets": self.facets,
            "search_controller": "Search::PropertiesController",
            "neighborhood": None,
            "street": None,
            "searchType": "city",
            "school": None,
            "school_district": None,
            "university": None,
            "park": None,
            "pos": None,
            "searchFacetsToDTM": ["price_min", "keywords"],
            "searchFeaturesToDTM": [],
            "types": ["property"],
            "page_size": 44,
            "viewport_height": 0,
            "pin_height": 25,
            "limit": 0}
        return data

    def get_headers_for_ads(self, url: str) -> dict:
        """Get headers to parse ads"""
        authority = self.authority
        method = 'GET'
        path = url.split('.com')[1]
        scheme = self.scheme
        accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,' \
                 'application/signed-exchange;v=b3;q=0.9'
        accept_encoding = self.accept_encoding
        accept_language = self.accept_language
        sec_fetch_mode = 'navigate'
        sec_fetch_site = 'none'
        sec_fetch_user = '71'
        user_agent = self.user_agent
        upgrade_insecure_requests = '1'
        cookie = self.cookie

        h = {'user-agent': user_agent,
             'authority': authority,
             'method': method,
             'path': path,
             'scheme': scheme,
             'accept': accept,
             'accept-encoding': accept_encoding,
             'accept-language': accept_language,
             'cookie': cookie,
             'sec-fetch-mode': sec_fetch_mode,
             'sec-fetch-site': sec_fetch_site,
             'sec-fetch-user': sec_fetch_user,
             'upgrade-insecure-requests': upgrade_insecure_requests
             }
        return h


a = {"url": "https://www.realtor.com/realestateandhomes-detail/4319-SW-Fairview-Circus_Portland_OR_97221_M12182-33400",
     "streetAddress": "4319 SW Fairview Circus,",
     "city_id": "Portland",
     "state": "OR",
     "postal_code": "97221",
     "price": "799000",
     "sqft": "3,156",
     "sqft_lot": "9,583",
     "area": "",
     "beds": "3",
     "baths": "2.5",
     "description": "Architect Marvin Witt captured what a NW Regional home is all about in this one-owner custom home; light-filled voluminous spaces harmonize with Mother Nature. Big picture & clerestory windows frame outlooks of the Hoyt Arboretum in every room. Thoughtfully placed decks, a breakfast nook sunroom and lower level patio invite the outdoors in. Unspoiled, original design married with quality materials creates a lifestyle opportunity on a quite lane with only 15 homes in the SW Hills. [Home Energy Score = 1. HES Report at",
     "additional_details": {
         "Appliances": "Microwave, Cook Island, Dishwasher, Disposal, Pantry, Built-in Oven, Free-Standing Refrigerator, Cooktop, Stainless Steel Appliance(s)",
         "Other Rooms": "Basement Features: Exterior Entry, Storage Space, Extra Room-1 Sqft: 221, Extra Room-2 Sqft: 136, Extra Room-3 Sqft: 210, Extra Room-1 Level: Main, Extra Room-2 Level: Main, Extra Room-3 Level: Lower, Living Room Level: Main, Extra Room-1 Features: Additional Room1 Features: Sliding Doors, Tile Floor, Additional Room Features: Bathroom, Built-in Features, Bookcases, Deck, Hardwood Floors, Sliding Doors, Tile Floor, Vaulted Ceiling(s), Wall to Wall Carpet, Closet, Extra Room-2 Features: Bathroom, Deck, Hardwood Floors, Sliding Doors, Vaulted Ceiling(s), Closet, Extra Room-3 Features: Built-in Features, Bookcases, Wall to Wall Carpet, Extra Room-1 Description: Additional Room1 Description: Sun Room, Additional Rooms: Den, Entry, Sun Room, Extra Room-2 Description: Entry, Extra Room-3 Description: Den",
         "Heating and Cooling": "Fireplace Features: Wood Burning, Heating Features: Forced Air - 95+%",
         "Interior Features": "Central Vacuum, Garage Door Opener, Hardwood Floors, Laundry, Tile Floor, Vaulted Ceiling(s), Washer/Dryer, Wall to Wall Carpet, High Ceilings, Soaking Tub",
         "Land Info": "Lot Description: Sloped, Trees, Wooded, Green Belt, Lot Size Acres: 0.22, Lot Size Dimensions: Lot Size: 7, 000 to 9, 999 SqFt",
         "Garage and Parking": "Number of Garage Spaces: 2, Garage Description: Attached, Garage Features: Garage: Attached, Parking Features: Driveway",
         "Homeowners Association": "Association: No, Calculated Total Monthly Association Fees: 0",
         "School Information": "Elementary School: Ainsworth, High School: Lincoln, Middle School: West Sylvan",
         "Other Property Info": "Source Listing Status: Active, County: Multnomah, Source Property Type: Single Family Residence, Area: Portland West, Raleigh Hills, Source Neighborhood: SYLVAN HIGHLANDS, Security Features: Security Gate, Security System Owned, Subdivision: SYLVAN HIGHLANDS, Source System Name: C2C",
         "Building and Construction": "Square Feet Living: 3156, Year Built: 1979, Building Exterior Type: Wood Siding, Energy Information: Forced Air - 95+%, Double Pane Windows, Property Age: 41, Property Condition: Resale, Roof: Composition, House Style: Custom Style, NW Contemporary, Total Area Main: 1247, Window Features: Double Pane Windows",
         "Utilities": "Sewer: Public Sewer, Water Source: Public",
         "Legal and finance": "HOA Frequency: Monthly/0, HOA fee: $0"},
     "images": {"img1": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m657312347xd-w1020_h770_q80.jpg",
                "img2": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m371259339xd-w1020_h770_q80.jpg",
                "img3": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m3347058014xd-w1020_h770_q80.jpg",
                "img4": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m1672853623xd-w1020_h770_q80.jpg",
                "img5": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m2291599086xd-w1020_h770_q80.jpg",
                "img6": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m147857402xd-w1020_h770_q80.jpg",
                "img7": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m932168619xd-w1020_h770_q80.jpg",
                "img8": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m3263871787xd-w1020_h770_q80.jpg",
                "img9": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m562375069xd-w1020_h770_q80.jpg",
                "img10": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m2789152586xd-w1020_h770_q80.jpg",
                "img11": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m857488565xd-w1020_h770_q80.jpg",
                "img12": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m1652292593xd-w1020_h770_q80.jpg",
                "img13": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m1552144040xd-w1020_h770_q80.jpg",
                "img14": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m4262704414xd-w1020_h770_q80.jpg",
                "img15": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m1686843532xd-w1020_h770_q80.jpg",
                "img16": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m117515608xd-w1020_h770_q80.jpg",
                "img17": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m2193717xd-w1020_h770_q80.jpg",
                "img18": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m2024346504xd-w1020_h770_q80.jpg",
                "img19": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m2116604475xd-w1020_h770_q80.jpg",
                "img20": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m3358696521xd-w1020_h770_q80.jpg",
                "img21": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m1593341834xd-w1020_h770_q80.jpg",
                "img22": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m1024854651xd-w1020_h770_q80.jpg",
                "img23": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m2950056083xd-w1020_h770_q80.jpg",
                "img24": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m4182353005xd-w1020_h770_q80.jpg",
                "img25": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m1464652827xd-w1020_h770_q80.jpg",
                "img26": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m3516313966xd-w1020_h770_q80.jpg",
                "img27": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m993301227xd-w1020_h770_q80.jpg",
                "img28": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m3465166618xd-w1020_h770_q80.jpg",
                "img29": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m409954440xd-w1020_h770_q80.jpg",
                "img30": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m345951574xd-w1020_h770_q80.jpg",
                "img31": "https://ap.rdcpix.com/fd4f66d8bc2886415ae735624feae2d5l-m3991363451xd-w1020_h770_q80.jpg"},
     "source_id": 1, "property_type": "single_family", "status": "for_sale"}
